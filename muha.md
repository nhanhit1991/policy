Điều Khoản Sử Dụng Website Thương Mại Điện Tử - Ứng Dụng MUHA
 
I.CHÍNH SÁCH BẢO MẬT
Mục đích
MUHA thông qua Tuyên bố về Quyền riêng tư này thông báo cho khách hàng các chính sách liên quan đến thông tin được thu thập từ trang web/ứng dụng này. Khi khách hàng truy cập vào trang web/ứng dụng của chúng tôi, nghĩa là khách hàng đồng ý với các điều khoản này.
Trang web/ứng dụng có quyền thay đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Quy định và Điều kiện sử dụng vào bất cứ lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên trang web/ứng dụng mà không cần thông báo trước. Khi khách hàng tiếp tục sử dụng trang web/ứng dụng, sau khi các thay đổi về Quy định và Điều kiện được đăng tải, có nghĩa là khách hàng chấp nhận với những thay đổi đó. Khách hàng vui lòng kiểm tra thường xuyên để cập nhật những thay đổi của chúng tôi.
Tự động thu thập thông tin ẩn danh
Khi khách hàng truy cập trang web/ứng dụng MUHA, một số thông tin ẩn danh về lượt truy cập của khách hàng sẽ tự động được ghi lại, có thể bao gồm thông tin về loại trình duyệt khách hàng sử dụng, tên máy chủ và địa chỉ IP mà khách hàng truy cập Internet, ngày và giờ khách hàng truy cập trang web/ứng dụng, các trang khách hàng truy cập khi ở trang web/ứng dụng MUHA và địa chỉ Internet của trang web (nếu có) mà từ đó khách hàng liên kết trực tiếp đến trang web/ứng dụng MUHA. Thông tin này không phải là thông tin cá nhân.
Thông tin cá nhân
Thông tin nhận dạng cá nhân là những thông tin liên quan đến khách hàng như tên, địa chỉ, số điện thoại/fax, số an sinh xã hội, địa chỉ email hoặc bất kỳ thông tin nào khách hàng gửi đến MUHA để xác định riêng từng cá nhân khách hàng.
www.muha.vn - ứng dụng MUHA sẽ không thu thập bất kỳ thông tin nhận dạng cá nhân nào về khách hàng trừ khi khách hàng cung cấp. Do đó, nếu khách hàng không muốn MUHA có được thông tin nhận dạng cá nhân nào, vui lòng đừng gửi nó.
Khách hàng có thể truy cập và duyệt trang web www.muha.vn/ứng dụng mà không cần tiết lộ thông tin nhận dạng cá nhân.
MUHA có thể thu thập thông tin nhận dạng cá nhân của khách hàng từ trang web/ứng dụng của mình bằng các phương pháp sau:
Mẫu đăng ký
Nếu khách hàng muốn nhận thông tin các chương trình khuyến mãi, hãy trở thành người dùng của trang web www.muha.vn/ứng dụng MUHA bằng cách điền vào biểu mẫu đăng ký trên trang web - ứng dụng. Biểu mẫu này yêu cầu một số thông tin nhận dạng cá nhân có thể bao gồm tên, địa chỉ email, địa chỉ bưu điện, số điện thoại, khu vực quan tâm, mức sử dụng sản phẩm và/hoặc mật khẩu cá nhân duy nhất của khách hàng.
Giao dịch và hoạt động
Nếu khách hàng là người dùng đã đăng ký hoặc khách hàng thực hiện giao dịch thông qua trang web www.muha.vn/ứng dụng MUHA, MUHA sẽ thu thập thông tin các giao dịch và hoạt động khác của khách hàng trên trang web/ứng dụng. Thông tin này có thể bao gồm khu vực của trang web nơi khách hàng truy cập, loại giao dịch, nội dung khách hàng xem, tải xuống hoặc gửi đi, số tiền giao dịch, thông tin thanh toán, giao hàng cũng như loại hàng, số lượng và giá của hàng hóa, dịch vụ khách hàng trao đổi...
Email và các thông tin liên lạc tự nguyện khác
Khách hàng có thể liên lạc với MUHA qua email, thông qua trang web www.muha.vn/ứng dụng, qua điện thoại, bằng văn bản hoặc các phương tiện khác. Chúng tôi thu thập các thông tin liên lạc này và thông tin đó có thể được nhận dạng cá nhân.
Sử dụng thông tin
MUHA có thể sử dụng thông tin nhận dạng cá nhân thu thập thông qua trang web/ứng dụng của mình chủ yếu cho các mục đích như:
Giúp thiết lập và xác minh danh tính của người dùng
Mở, duy trì, quản lý và phục vụ tài khoản hoặc tư cách thành viên của người dùng
Xử lý, bảo trì hoặc thực thi các giao dịch và gửi các liên lạc liên quan
Cung cấp dịch vụ và hỗ trợ cho người dùng
Cải thiện trang web/ứng dụng bao gồm điều chỉnh trang web/ứng dụng theo sở thích của người dùng
Cung cấp cho người dùng thông tin cập nhật về sản phẩm hoặc dịch vụ, thông báo khuyến mãi, phiếu mua hàng, các thông tin khác về MUHA và các chi nhánh
Trả lời các câu hỏi, ý kiến ​​và hướng dẫn khách hàng
Duy trì tính bảo mật và tính toàn vẹn của hệ thống MUHA
MUHA sử dụng thông tin duyệt web ẩn danh được thu thập tự động bởi các máy chủ chủ yếu quản lý và cải thiện trang web/ứng dụng của mình. MUHA cũng có thể sử dụng thông tin ẩn danh tổng hợp để cung cấp thông tin về trang web/ứng dụng của mình cho các nhà quảng cáo, đối tác kinh doanh tiềm năng. Thông tin này không thể nhận dạng cá nhân.
Tiết lộ thông tin
MUHA không cung cấp thông tin nhận dạng cá nhân cho các bên thứ ba không liên kết để họ sử dụng tiếp thị trực tiếp cho khách hàng. MUHA có thể sử dụng các công ty không liên kết để giúp duy trì và vận hành trang web/ứng dụng của mình; hoặc vì các lý do khác liên quan đến hoạt động kinh doanh mà các công ty này có thể nhận được thông tin nhận dạng cá nhân của khách hàng cho mục đích đó.
MUHA cũng có thể tiết lộ thông tin nhận dạng cá nhân về khách hàng liên quan đến các yêu cầu pháp lý, chẳng hạn như trát đòi hầu tòa, yêu cầu điều tra của chính phủ hoặc được luật pháp cho phép. Sau cùng, khi kinh doanh phát triển, MUHA có thể bán hoặc mua tài sản của công ty và trong các giao dịch như vậy, thông tin khách hàng có thể là một trong những tài sản kinh doanh được chuyển giao.
Liên kết khác
Trang web www.muha.vn/ứng dụng MUHA có thể chứa liên kết đến các trang web khác bao gồm các trang web của nhà quảng cáo hoặc nhà cung cấp nội dung bên thứ ba cung cấp tải xuống trên www.muha.vn. Xin lưu ý rằng www.muha.vn không chịu trách nhiệm về vấn đề bảo mật hoặc nội dung của các trang web khác. Các trang web có thể truy cập được bằng siêu liên kết (hyperlinks) từ trang web www.muha.vn có thể sử dụng cookie. MUHA khuyến khích khách hàng đọc các điều khoản về quyền riêng tư được cung cấp bởi các trang web khác trước khi khách hàng cung cấp thông tin nhận dạng cá nhân cho họ.
Bảo mật
MUHA duy trì các biện pháp bảo vệ vật lý, điện tử và thủ tục để giúp bảo vệ thông tin nhận dạng cá nhân. Nếu giao dịch được thực hiện trên trang web/ứng dụng, thông tin giao dịch được truyền đến MUHA dưới dạng mã hóa bằng cách sử dụng các kết nối Lớp cổng bảo mật (SSL) tiêu chuẩn của ngành để giúp bảo vệ thông tin đó khỏi bị chặn. Tuy nhiên, xin lưu ý rằng bất kỳ email hoặc bất kỳ thông tin nào khác mà khách hàng gửi qua Internet không thể được bảo vệ hoàn toàn khỏi bị chặn trái phép.
Chọn /Bỏ chọn tham gia
Để cung cấp dịch vụ cho khách hàng, MUHA sẽ gửi cho khách hàng thông tin liên lạc liên quan đến giao dịch, bảo mật hoặc quản trị trang web/ứng dụng của khách hàng. Đôi khi, MUHA cũng có thể gửi cho khách hàng các tin nhắn hoặc thông tin cập nhật khác về MUHA, các chi nhánh hoặc các hoạt động quảng bá khác. Nếu khách hàng không muốn nhận thông tin liên lạc không liên quan đến giao dịch hoặc bảo mật từ MUHA, vui lòng gửi email chứa yêu cầu của khách hàng đến dịch vụ khách hàng.
Chất lượng dữ liệu / Truy cập
MUHA cho phép khách hàng thay đổi, cập nhật hoặc xóa thông tin khách hàng có thể cung cấp trong biểu mẫu đăng ký tùy chọn của mình. Nếu khách hàng muốn thay đổi, cập nhật hoặc xóa thông tin cá nhân của mình, vui lòng gửi email chứa yêu cầu của khách hàng đến dịch vụ khách hàng.
 
1. ĐẶT HÀNG VÀ XÁC NHẬN ĐƠN HÀNG
Khi khách hàng đặt hàng tại www.muha.vn/ứng dụng MUHA, chúng tôi sẽ nhận được yêu cầu đặt hàng và xác nhận đơn hàng của khách hàng qua điện thoại.
Để yêu cầu đặt hàng được xác nhận nhanh chóng, khách hàng vui lòng cung cấp đúng và đầy đủ các thông tin liên quan đến việc giao nhận, hoặc các điều khoản và điều kiện của chương trình khuyến mãi (nếu có) mà khách hàng tham gia.
II. QUY ĐỊNH VỀ GIÁ
Giá cả sản phẩm được niêm yết tại www.muha.vn/ứng dụng MUHA là giá bán cuối cùng đã bao gồm thuế Giá trị gia tăng (VAT). Giá cả của sản phẩm có thể thay đổi tùy thời điểm và chương trình khuyến mãi kèm theo. Phí vận chuyển hoặc Phí thực hiện đơn hàng sẽ được áp dụng theo chính sách giao hàng.
Mặc dù chúng tôi cố gắng tốt nhất để bảo đảm rằng tất cả các thông tin và giá hiển thị là chính xác đối với từng sản phẩm, đôi khi sẽ có một số trường hợp bị lỗi hoặc sai sót. Nếu chúng tôi phát hiện lỗi về giá của bất kỳ sản phẩm nào trong đơn hàng của khách hàng, chúng tôi sẽ thông báo cho khách hàng trong thời gian sớm nhất có thể và gửi đến khách hàng lựa chọn để xác nhận lại đơn hàng với giá chính xác hoặc hủy đơn hàng. Nếu chúng tôi không thể liên lạc với khách hàng, đơn hàng sẽ tự động hủy trên hệ thống và lệnh hoàn tiền sẽ được thực hiện (nếu đơn hàng đã được thanh toán trước).

2. QUY TRÌNH GIAO DỊCH
Bước 1: Tìm sản phẩm cần mua
Bước 2: Xem giá và thông tin chi tiết sản phẩm, nếu khách hàng đồng ý muốn mua hàng, khách hàng ấn thêm vào giỏ hàng.
Bước 3: Tại Form giỏ hàng Click vào nút Tiếp Tục để tiến hành đặt hàng
Bước 4: Khách hàng điền đầy đủ thông tin mua hàng và xác nhận đơn hàng.
Khách hàng hàng đang sử dụng thẻ ATM nội địa hoặc thẻ tín dụng Visa, Master khách hàng có thể thanh toán đơn hàng bằng cách click vào “Thanh toán trực tuyến (ATM, VISA, MASTER)” và làm theo hướng dẫn của ngân hàng.
Bước 5: Sau khi nhận đơn hàng của người mua, MUHA sẽ liên lạc với khách hàng qua thông tin số điện khách hàng hàng cung cấp bằng tổng đài 1800 088 895 hoặc đầu số khác để xác thực thông tin đơn hàng.
Bước 6: MUHA giao hàng tận nhà đến cho khách hàng.

3. QUY TRÌNH THANH TOÁN
Người mua có thể tham khảo các phương thức thanh toán sau đây và lựa chọn áp dụng phương thức phù hợp:
Cách 1: Thanh toán sau (COD – giao hàng và thu tiền tận nơi).
Bước 1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin.
Bước 2: Người mua xác thực đơn hàng (điện thoại).
Bước 3: MUHA xác nhận thông tin Người mua.
Bước 4: MUHA chuyển hàng.
Bước 5: Người mua nhận hàng và thanh toán bằng tiền mặt.

Cách 2: Thanh toán online qua thẻ tín dụng, chuyển khoản.
Bước 1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin.
Bước 2: Người mua xác thực đơn hàng (điện thoại).
Bước 3: MUHA xác nhận thông tin Người mua.
Bước 4: Người mua chuyển khoản thanh toán.
Bước 5: MUHA chuyển hàng.
Bước 6: Người mua nhận hàng
4. ĐẢM BẢO AN TOÀN GIAO DỊCH
Yêu cầu đảm bảo an toàn giao dịch đối với người mua:
Ban quản lý đã sử dụng các dịch vụ để bảo vệ thông tin và việc thanh toán của người tiêu dùng. Để đảm bảo các giao dịch được tiến hành thành công, hạn chế tối đa rủi ro có thể phát sinh, yêu cầu các Thành viên tham gia website Thương mại điện tử/ứng dụng di động MUHA lưu ý và tuân thủ các nội dung cam kết sau đây:
Bất cứ một sự gian lận và sử dụng trái phép thẻ thanh toán bởi ai đó để mua hàng từ khách hàng, chúng tôi sẽ hoàn lại số tiền chúng tôi đã nhận của khách hàng chỉ khi khách hàng thông báo đến Nhà Phát Hành Thẻ và liên lạc ngay với chúng tôi khi khách hàng phát hiện việc không sử dụng được Thẻ Thanh Toán.
Khách hàng không nên đưa thông tin chi tiết về việc thanh toán với bất kỳ ai bằng email, chúng tôi không chịu trách nhiệm về những mất mát khách hàng có thể gánh chịu trong việc trao đổi thông tin của khách hàng qua Internet hoặc email.
Khách hàng tuyệt đối không sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. Nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm phạm hệ thống website/ứng dụng. Mọi vi phạm sẽ bị xử lý theo Quy chế và quy định của pháp luật.
Mọi thông tin giao dịch được bảo mật, trừ trường hợp buộc phải cung cấp khi Cơ quan pháp luật yêu cầu.
Cơ chế đảm bảo an toàn giao dịch của Ban Quản lý Website Thương mại điện tử
MUHA chủ động cung cấp giải pháp thanh toán đảm bảo qua cổng thanh toán Onepay và thanh toán khi nhận hàng (COD) nhằm bảo vệ tốt nhất cho khách hàng tránh khỏi những rắc rối trong thanh toán khi mua hàng trực tuyến.
Quy trình thanh toán đảm bảo qua cổng thanh toán Onepay: Công ty triển khai hệ thống thanh toán trực tuyến thông qua cổng thanh toán trực tuyến OnePAY thuộc công ty cổ phần thương mại và dịch vụ trực tuyến Onepay đảm bảo các tiêu chuẩn của hệ thống tài chính quốc tế như PCI DSS của PCI Security Standards Council, 3D-Secure của Visa, MasterCard và JCB…. bảo đảm an toàn số liệu, thông tin về giao dịch và tài khoản của khách hàng đáp ứng các quy định của Ngân hàng/hoặc quy định của pháp luật có liên quan trong hoạt động cung cấp dịch vụ hỗ trợ thanh toán. Quy trình bảo mật thanh toán của khách hàng, xử lý khiếu nại được thực hiện như sau:
An Toàn Bảo mật
- Tiền của khách hàng sẽ chỉ được chuyển cho Nhà cung cấp sau khi khách hàng xác nhận hoàn tất giao dịch.
- Khách hàng sẽ nhanh chóng được hoàn lại 100% giá trị giao dịch khi không nhận được hàng.
- Khách hàng có quyền được khiếu nại và phân xử khi không nhận được hàng hoặc nhận không đúng hàng.
- Mọi thông tin giao dịch, thông tin tài khoản và thẻ của khách hàng đều được bảo mật bởi công nghệ bảo mật cao nhất hiện có trong thương mại điện tử.
- Hệ thống chống virus giúp bảo vệ thông tin của khách hàng tốt nhất khỏi những phần mềm gián điệp, virus, Trojan.
Thanh toán khi nhận hàng (COD): Khách hàng có thể lựa chọn hình thức thanh toán khi nhận hàng (COD) cho giao dịch của mình. Nếu khách hàng không hài lòng về sản phẩm nhận được, kể cả sau khi khách hàng đã nhận hàng và thanh toán, khách hàng vẫn có thể tiến hành khiếu nại và trả sản phẩm theo chính sách đổi trả hàng của công ty.
III. GIẢI QUYẾT TRANH CHẤP KHIẾU NẠI
MUHA tôn trọng và nghiêm túc thực hiện các quy định của pháp luật về bảo vệ quyền lợi của người mua (người tiêu dùng). Vì vậy, sản phẩm cung cấp trên phải đầy đủ, chính xác, trung thực và chi tiết các thông tin liên quan đến sản phẩm. Mọi hành vi lừa đảo, gian lận trong kinh doanh đều bị lên án và phải chịu hoàn toàn trách nhiệm trước pháp luật.
Cơ chế tiếp nhận và giải quyết khiếu nại của khách hàng khi thông tin cá nhân bị sử dụng sai mục đích hoặc phạm vi:
Khi phát hiện những sai phạm trong việc khai thác và sử dụng thông tin người dùng không đúng mục đích, người dùng có thể liên hệ qua những cách sách đây:
Liên hệ trực tiếp hoặc gửi văn bản về văn phòng làm việc của Công Ty Cổ Phần MUHA, địa chỉ văn phòng: Số 73 - 75 đường Trần Nguyên Hãn, P.Dĩ An, TP Dĩ An, Tỉnh Bình Dương
Liên hệ với Bộ phận CSKH của MUHA theo: Hotline: 1800 088 895 / Email: info@muha.vn
MUHA cam kết sẽ phản hồi ngay lập tức trong vòng 48 tiếng để cùng Người dùng thống nhất phương án giải quyết.
IV. QUYỀN NGHĨA VỤ CỦA BAN QUẢN LÝ VÀ THÀNH VIÊN
Ban quản lý website thương mại điện tử www.muha.vn/ứng dụng MUHA
Xây dựng và công bố công khai trên website thương mại điện tử www.muha.vn/ứng dụng MUHA, theo dõi và đảm bảo việc thực hiện quy chế nghiêm túc Đăng ký sàn giao dịch thương mại điện tử với cơ quan nhà nước có thẩm quyền. Xây dựng Quy chế hoạt động, Quy định, Hướng dẫn các quy trình giao dịch…cho người dùng.
Đảm bảo hạ tầng thanh toán trực tuyến an toàn cho khách hàng mua hàng, sử dụng sản phẩm trên www.muha.vn/ứng dụng MUHA cho phép hiển thị, lưu trữ các thông tin liên quan đến tên hàng hóa, dịch vụ, số lượng, chủng loại hàng hóa khách hàng đã đăng ký mua; tổng giá trị hàng hóa đã mua và các nội dung liên quan đến phương thức thanh toán
Lưu trữ và đảm bảo an toàn đối với toàn bộ các thông tin cá nhân của khách hàng. Cam kết không sử dụng các thông tin này vào bất cứ mục đích thương mại nào khác ngoài phạm vi Quy chế hoạt động của www.muha.vn/ứng dụng MUHA.
Cung cấp thông tin chi tiết sản phẩm, dịch vụ đặt hàng chương trình khuyến mãi cho người dùng.
Bán hàng đúng như thông tin đã cung cấp.
Giao hàng tại các cửa hàng hoặc theo thỏa thuận.
Thực hiện theo các chính sách đã công bố trên web.
Thành Viên (Người Mua)
Đăng ký tài khoản và hưởng các chính sách khuyến mãi của công ty. Đối với thành viên sẽ được cấp một tên đăng ký và mật khẩu riêng để được vào sử dụng trong việc quản lý những giao dịch tại www.muha.vn/ứng dụng MUHA.
Khách hàng có quyền đóng góp ý kiến cho website thương mại điện tử www.muha.vn/ứng dụng MUHA trong quá trình hoạt động. Các kiến nghị được gửi trực tiếp bằng thư hoặc email đến cho MUHA.
Thành viên sẽ tự chịu trách nhiệm về bảo mật và lưu giữ và mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình.
Không được hành động gây mất uy tín của website thương mại điện tử www.muha.vn/ứng dụng MUHA dưới mọi hình thức như gây mất đoàn kết giữa các thành viên bằng cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến những thông tin không có lợi cho uy tín của website thương mại điện tử www.muha.vn/ứng dụng MUHA.
Tìm hiểu đầy đủ thông tin sản phẩm trước khi mua hàng.
Kiểm tra hàng hóa.
Thanh toán đầy đủ tiền hàng.
5. CHÍNH SÁCH BẢO MẬT
Mục đích và phạm vi thu thập
Việc thu thập dữ liệu chủ yếu trên website thương mại điện tử www.muha.vn/ứng dụng bao gồm: email, điện thoại, tên đăng nhập, mật khẩu đăng nhập, địa chỉ khách hàng (thành viên). Đây là các thông tin mà MUHA yêu cầu thành viên cung cấp bắt buộc khi đăng ký sử dụng dịch vụ và để MUHA liên hệ xác nhận khi khách hàng đăng ký sử dụng dịch vụ trên website/ứng dụng nhằm đảm bảo quyền lợi cho người tiêu dùng. 
Trong quá trình giao dịch thanh toán tại website thương mại điện tử www.muha.vn/ứng dụng MUHA, MUHA chỉ lưu giữ thông tin chi tiết về đơn hàng đã thanh toán của thành viên, các thông tin về số tài khoản ngân hàng của thành viên sẽ không được lưu giữ. MUHA cũng sẽ sử dụng cả thông tin nhận diện cá nhân của thành viên và một số thông tin nhận diện phi cá nhân (như cookies, địa chỉ IP, loại trình duyệt, ngày tổng số, v.v.) để gia tăng khả năng đáp ứng của MUHA về phương diện các Trang và Dịch Vụ, cũng như về phát triển những chức năng, tính năng và các dịch vụ mới theo những xu hướng và sở thích đang diễn tiến. Các thành viên sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình. Ngoài ra, thành viên có trách nhiệm thông báo kịp thời cho website thương mại điện tử www.muha.vn/ứng dụng về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.
Phạm vi sử dụng thông tin
Website thương mại điện tử www.muha.vn/ứng dụng sử dụng thông tin thành viên cung cấp để:
Cung cấp các dịch vụ đến thành viên.
Gửi các thông báo về các hoạt động trao đổi thông tin giữa thành viên và website thương mại điện tử www.muha.vn/ứng dụng MUHA.
Ngăn ngừa các hoạt động phá hủy tài khoản người dùng của thành viên hoặc các hoạt động giả mạo thành viên.
Liên lạc và giải quyết với thành viên trong những trường hợp đặc biệt.
Không sử dụng thông tin cá nhân của thành viên ngoài mục đích xác nhận và liên hệ có liên quan đến giao dịch tại website thương mại điện tử www.muha.vn/ứng dụng MUHA.
Trong trường hợp có yêu cầu của pháp luật: MUHA có trách nhiệm hợp tác cung cấp thông tin cá nhân thành viên khi có yêu cầu từ cơ quan tư pháp bao gồm: Viện kiểm sát, tòa án, cơ quan công an điều tra liên quan đến hành vi vi phạm pháp luật nào đó của khách hàng. Ngoài ra, không ai có quyền xâm phạm vào thông tin cá nhân của thành viên.
Thời gian lưu trữ thông tin
Dữ liệu cá nhân của thành viên sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân thành viên sẽ được bảo mật trên máy chủ của website thương mại điện tử www.muha.vn/ứng dụng MUHA.
Địa chỉ của đơn vị thu thập và quản lý thông tin
Công Ty Cổ Phần MUHA
Địa chỉ văn phòng: Số 73 - 75 đường Trần Nguyên Hãn, P.Dĩ An, TP Dĩ An, Tỉnh Bình Dương.
Điện thoại: 0976 610 108 - Hotline: 1800 088 895
Email: info@muha.vn
Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân của mình.
Thành viên có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ thông tin cá nhân của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin cá nhân hoặc yêu cầu MUHA thực hiện việc này. Thành viên có quyền gửi khiếu nại về việc lộ thông tin các nhân cho bên thứ 3 đến Ban quản trị của website thương mại điện tử www.muha.vn/ứng dụng MUHA. Khi tiếp nhận những phản hồi này, MUHA sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và hướng dẫn thành viên khôi phục và bảo mật lại thông tin. Email: info@muha.vn Số điện thoại: 1800 088 895.
 
Cam kết bảo mật thông tin cá nhân khách hàng
Thông tin cá nhân của thành viên trên MUHA được chúng tôi cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân. Việc thu thập và sử dụng thông tin của mỗi thành viên chỉ được thực hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.
Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về thông tin cá nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.
Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân thành viên, MUHA sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.
Bảo mật tuyệt đối mọi thông tin giao dịch trực tuyến của Thành viên bao gồm thông tin hóa đơn kế toán chứng từ số hóa tại khu vực dữ liệu trung tâm an toàn cấp 1 của MUHA.
Ban quản lý MUHA yêu cầu các cá nhân khi đăng ký/mua hàng là thành viên, phải cung cấp đầy đủ thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email, số điện thoại… và chịu trách nhiệm về tính pháp lý của những thông tin trên. Ban quản lý MUHA không chịu trách nhiệm cũng như không giải quyết mọi khiếu nại có liên quan đến quyền lợi của Thành viên đó nếu xét thấy tất cả thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính xác.
6. ĐIỀU KHOẢN ÁP DỤNG
Mọi tranh chấp phát sinh giữa Sàn giao dịch điện tử www.muha.vn/ứng dụng MUHA và thành viên sẽ được giải quyết trên cơ sở thương lượng. Trường hợp không đạt được thỏa thuận như mong muốn, một trong hai bên có quyền đưa vụ việc ra Tòa án nhân dân có thẩm quyền tại Thành phố Hồ Chí Minh để giải quyết
Quy chế của Sàn giao dịch điện tử www.muha.vn/ứng dụng MUHA chính thức có hiệu lực thi hành kể từ ngày ký Quyết định ban hành kèm theo Quy chế này. Sàn giao dịch điện tử www.muha.vn/ứng dụng MUHA có quyền và có thể thay đổi Quy chế này bằng cách thông báo lên Sàn giao dịch điện tử www.muha.vn/ứng dụng MUHA cho các thành viên biết. Quy chế sửa đổi có hiệu lực kể từ ngày Quyết định về việc sửa đổi Quy chế có hiệu lực. Việc thành viên tiếp tục sử dụng dịch vụ sau khi Quy chế sửa đổi được công bố và thực thi đồng nghĩa với việc họ đã chấp nhận Quy chế sửa đổi này.
7. ĐIỀU KHOẢN CAM KẾT
Địa chỉ liên lạc chính thức của website thương mại điện tử www.muha.vn
Website thương mại điện tử www.muha.vn
Ứng dụng MUHA
Công ty/Tổ chức: Công Ty Cổ Phần MUHA
Địa chỉ văn phòng: Số 73 - 75 đường Trần Nguyên Hãn, P. Dĩ An, TP Dĩ An, tỉnh Bình Dương.
Điện thoại: 097 661 01 08 - Hotline: 1800 088 895 (Đặt hàng).
Email: info@muha.vn


